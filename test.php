
<!DOCTYPE html>
<!-- Microdata markup added by Google Structured Data Markup Helper. -->
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Religare Prod</title>
        <meta name="description" content="Religare Health insurance, the most trusted health insurance company in India; offers health & travel insurance to individuals, families & corporates." />
        <meta name="keywords" content="health insurance, medical insurance, health insurance in India, best health insurance in India, best health insurance, medical health insurance, health insurance companies in India" />
        <meta name="robots" content="Index, Follow" />
        <meta name="msnbot" content=" Index, Follow" />
        <meta name="owner" content="Religare Health Insurance"/>
        <meta name="Language" content="en-us" />
        <meta name="revisit-after" content="3 days" />
        <meta name="MSSmartTagsPreventParsing" content="true" />
        <link rel="canonical" href="http://www.religarehealthinsurance.com/"/>
        <meta name="alexaVerifyID" content="uDa4qYbiVdZzMVcTvPL4V0Usavg"/>
        <meta name="author" content= "religarehealthinsurance.com"/>
        <meta name="googlebot" content= "Index, Follow"/>
        <meta name="yahooseeker" content= "Index, Follow"/>
        <meta name="distribution" content= "india"/>
        <meta name="document-type" content="Public"/>
        <meta name="rating" content= "general"/>
        <meta name="google-site-verification" content="7Cdu_dZSqSjnUDerlYGwLDawUgeRLxwD45IweiGVC68" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
         <script>
window['adrum-start-time']= new Date().getTime();
</script>
<script src="http://www.religarehealthinsurance.com/cpjs/adrum.prod.js"></script>    

        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '1089386891152278');
            fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1089386891152278&ev=PageView&noscript=1"
                   /></noscript>
    <!-- End Facebook Pixel Code -->

<!-- determine if travel page -->
<script type="text/javascript">
  var travelPageBool = (window.location.href.indexOf("travel")>-1) ? true : false;
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
{'gtm.start': new Date().getTime(),event:'gtm.js'}
);var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer',"GTM-KLG5KP");</script>
<!-- End Google Tag Manager -->

<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=266673,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
/* DO NOT EDIT BELOW THIS LINE */
f=false,d=document;return{use_existing_jquery:function()
{return use_existing_jquery;}
,library_tolerance:function()
{return library_tolerance;}
,finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function()
{return f;}
,load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  if(travelPageBool){
     var _gaq = _gaq || [];
    _gaq.push(['_setAccount', "UA-33473576-1"]);
    _gaq.push(['_trackPageview']);
  }
  ga('create', "UA-33473576-1", 'auto');
  ga('send', 'pageview');
</script>

<script>
(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '884119644931791']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=884119644931791&amp;ev=PixelInitialized" /></noscript>
<script type="text/javascript">
(function() {
var hm = document.createElement('script'); hm.type ='text/javascript'; hm.async = true;
hm.src = ('++u-heatmap-it+log-js').replace(/[+]/g,'/').replace(/-/g,'.');
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hm, s);
})();
</script>

<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'434934'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded")
{return}
try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p)
{y([p])}
;y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KLG5KP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
<!-- End Google Tag Manager (noscript) -->

<link rel="stylesheet" type="text/css" href="http://www.religarehealthinsurance.com/cpcss/sprites.css?rv=1044" media="all">
<link rel="stylesheet" type="text/css" href="http://www.religarehealthinsurance.com/cpcss/datepiker.css?rv=1044">
<link rel="stylesheet" type="text/css" href="http://www.religarehealthinsurance.com/cpcss/intlTelInput.css?rv=1044">
<link rel="stylesheet" type="text/css" href="http://www.religarehealthinsurance.com/cpcss/common.css?rv=1044" media="all">
<link rel="stylesheet" type="text/css" href="http://www.religarehealthinsurance.com/cpcss/style.css?rv=1044">
<link rel="stylesheet" type="text/css" href="http://www.religarehealthinsurance.com/cpcss/stylesheet.css?rv=1044" media="all">
<link rel="manifest" href="https://my.religarehealthinsurance.com/notifyvisitors_push/chrome/manifest.json">
<script>
    if(navigator.appVersion.match(/msie [5-7]/i)){
        var test = "yes";

        if(test == 'yes'){
            location.href="http://www.religarehealthinsurance.com/oldwebsite/";
        }
    }

</script>
<!-- heatmap -->
    <script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0064/3749.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
{'gtm.start': new Date().getTime(),event:'gtm.js'}
);var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer',"GTM-KLG5KP");</script> -->
<!-- End Google Tag Manager -->

<!--[if lt IE 9]>
    <script src="cpjs/html5.js"></script>
    <link rel="stylesheet" type="text/css" href="cpcss/style_ie7.css">

<![endif]-->

 <!--[if lt IE 9]>
    <script src="cpjs/html5.js?rv=1044"></script>
    <link rel="stylesheet" type="text/css" href="cpcss/style_ie7.css?rv=1044">

<![endif]-->
<!--[if lt IE 9]>

<script type="text/javascript" src="cpjs/jquery.js?rv=1044"></script>   
<![endif]-->
<!--[if (IE 9)]><!--> <script src="cpjs/jquery.js?rv=1044"></script> <!--<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <script src="cpjs/jquery-1.4.4.min.js?rv=1044"></script> <!--<![endif]-->

<!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="cpjs/selectivizr-min.js?rv=1044"></script>
<![endif]-->




<div class="rightFloat">
    <div class="rightFloatIn" id="rightFloatInRequestCallBack"><a href="http://www.religarehealthinsurance.com/inc_request_callback.php?pageName=care" class="callmeback_popup"><div class="rightFloatLeft"><span class="sprite scrollIcon4_png"></span></div><div class="rightFloatRight"><strong>Request<br />A Call Back</strong></div><div class="cl"></div></a></div>
    <div class="rightFloatIn" id=""><div class="rightFloatLeft"><span class="sprite scrollIcon1_png"></span></div><div class="rightFloatRight"><strong>1860-500-4488<br />1800-200-4488</strong></div><div class="cl"></div></div>
    <div class="rightFloatIn"><a href="http://www.religarehealthinsurance.com/send_mail.php" class="send_mail"><div class="rightFloatLeft">
                <span class="sprite scrollIcon2_png"></span></div><div class="rightFloatRight"><strong style="line-height:39px;">Email</strong>
            </div><div class="cl"></div></a></div>
    <div class="rightFloatIn"><a href="http://www.religarehealthinsurance.com/tutorial.php"><div class="rightFloatLeft"><span class="sprite scrollIcon3_png"></span></span></div><div class="rightFloatRight"><strong style="line-height:39px;">Tutorial</strong></div></a><div class="cl"></div></div>
</div>

<div class="screenlock"></div>

<header> 
    <a href="./" class="logo"></a>

    <div class="navigation_in">
        <!--Main Nav Start Here-->
               <ul id="navigation" class="ulnav">
            <li>
                <a href="javascript:void(0);"  class="tnav">Travel&nbsp;Insurance test</a>
                <div class="drop-menu" style="display:none; height:460px;">
                    <div class="drop-menuIn">
                        <ul class="fl cat-main">
                            <li ><a href="http://www.religarehealthinsurance.com/travel-insurance-explore.html" class='active'><span class="sprite exploreImg_png"></span><br />
                                    International Travel Insurance</a>             

                                <div style="display: block; height:360px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>The world without a worry !</p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Region specific plans hence low cost and High features</li>
                                                    <li>No upper age limit. No per ailment limit</li>
                                                    <li>Automatic doubling of sum insured in case of hospitalization due to accident without any extra costs</li>
                                                    <li>Pre Existing disease & OPD coverage</li>
                                                    <li>Pay per day rates instead of slabs, hence low cost</li>
                                                    <li>Cashless claim settlement</li>
                                                    <li>Upgradation to business class</li>
                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="travel-insurance-explore.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>

                            <li> <a href="http://www.religarehealthinsurance.com/travel-insurance-student-explore.html" onClick="_gaq.push(['_trackEvent', 'mainnav1'])"><span class="sprite studentExploreImg_png"></span><br />
                                    International Travel & Health Insurance for students</a>
                                <div style="display: none; height:360px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>So, you can focus on your studies, and you'll only bring back happy memories</p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Comprehensive & extremely low cost travel insurance plans for Students going to international universities with benefits like Personal accident, travel inconvenience benefits</li>
                                                    <li>Loads of optional covers to customize your plan depending on University requirements</li>
                                                    <li>Need based coverage tenure- 1 month to 3 years</li>
                                                    <li>Flexible to choose your geographical scope of coverage</li>
                                                    <li>Sponsor Protection</li>
                                                    <li>Cashless claims settlement</li>

                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="travel-insurance-student-explore.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>  

                            <li ><a href="http://www.religarehealthinsurance.com/group-explore-filldetails" class=''><img src="http://www.religarehealthinsurance.com/cpimages/group-explore.png" class="header_images"><br />
                                    Group Travel Insurance</a>             

                                <div style="display: block; height:430px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>They say, travelling makes you a storyteller. We ensure your employees  only have happy stories to share. Give them the comfort of Group Explore our Travel Insurance on their next work-trip both International or Domestic. </p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Flexibility to select from a Single-Trip Policy or an Annual Multi –Trip Policy.</li>
                                                    <li>Wide range of product construct options available.</li>
                                                    <li>Coverage for Adventure Sports  Injury.</li>
                                                    <li>Coverage for both IPD &#38; OPD coverage. </li>
                                                    <li>Dedicated Travel Manager/Admin for efficient service delivery.</li>
                                                    <li>Coverage for Diagnostic tests such as Radiotherapy, Chemotherapy, Cancer Screening &#38; Mammography. </li>
                                                    <li>Non-medical benefits covered such as – Loss of Passport &#38; Check-in Baggage and delayed trip. </li>
                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="http://www.religarehealthinsurance.com/group-explore-filldetails" >Know More</a>         
                                    </div>

                                </div>
                            </li>
                        </ul>
                        <div class="cl"></div>
                    </div>
                </div>
            </li>

            <li>
                <a href="javascript:void(0);" class="tnav">Health Insurance</a>
                <div class="drop-menu" style="display:none; height:430px;">
                    <div class="drop-menuIn">
                        <ul class="fl cat-main">
                            <li><a href="http://www.religarehealthinsurance.com/buy-health-insurance-policy-plan-online.html"class='active'><span class="sprite careImg_png"></span><br />
                                    Comprehensive Health Insurance</a>             
                                <div style="display: block; height:397px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>With Religare as your insurer, its truly Ab health Hamesha! </p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Wide range of Sum Insured- up to Rs.</span> 6 cr with life-long renewability</li>
                                                    <li>Get Automatic recharge of sum insured if claim amount exhausts your coverage, at no extra cost</li>
                                                    <li>Upto 150% increase in sum insured with No Claim Bonus- Super</li>
                                                    <li>Annual health check-up for insured members- regardless of claims history</li>
                                                    <li>No pre-policy medical check-up for sum insured less than Rs. 25 lacs, till 50 years of age .</li>
                                                    <li>Cashless & Hassle-free direct claim settlement with us at 4900+ Hospitals</li>
                                                </ul>
                                                <div class="cl">
                                                    
                                                </div>
                                            </div>
                                        </div>  

                                        <a class="knowbuyBtn" href="buy-health-insurance-policy-plan-online.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>

                            <li> <a href="http://www.religarehealthinsurance.com/policy-buy-senior-citizens-health-insurance-online.html"  onClick="_gaq.push(['_trackEvent', 'mainnav1'])"><span class="sprite careFreedomImg_png"></span><br />
                                    Get Health Insurance without any Medical Check-ups</a>
                                <div style="display: none; height:397px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>Freedom to get Health Insurance without worrying about your current health status</p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>No pre-policy medical check-up, for all ages & sum insured with guaranteed life-long renewability</li>
                                                    <li>Only 2 year waiting period in case of Pre-existing diseases</li>
                                                    <li>Get Automatic recharge of sum insured if claim amount exhausts your coverage, at no extra cost</li>
                                                    <li>Annual health check-up for insured members- regardless of claims history</li>
                                                    <li>Cashless & Hassle-free direct claim settlement with us at 4900+ Hospitals</li> 

                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="policy-buy-senior-citizens-health-insurance-online.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>  

                            <li> <a href="http://www.religarehealthinsurance.com/buy-top-up-medical-insurance-policy.html" onClick="_gaq.push(['_trackEvent', 'mainnav1'])"><span class="sprite enhanceImg_png"></span><br />
                                    High Deductible Health Insurance</a>
                                <div style="display: none; height:375px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>Get enhanced coverage for greater protection</p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Flexible choice of deductible with wide range of Sum Insured options- upto <span class="WebRupee">Rs.</span> 60 lacs (Deductible + Sum Insured)</li>
                                                    <li>Annual health check-up for insured members- regardless of claims history</li> 
                                                    <li>Cashless & Hassle-free direct claim settlement with us at 4900+ Hospitals </li>
                                                    <li>Avail select medical treatment anywhere in the world with Enhance anywhere feature</li>
                                                    <li>No pre-policy medical check-up up to Rs.</span>40 lacs Sum Insured till 50 years of age.</li>
                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="buy-top-up-medical-insurance-policy.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>

                            <li> <a href="http://www.religarehealthinsurance.com/buy-maternity-health-insurance-plan.html" onClick="_gaq.push(['_trackEvent', 'mainnav1'])"><span class="sprite joyImg_png"></span><br />
                                    Maternity & New born baby cover with Health Insurance</a>
                                <div style="display: none; height:397px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>A perfect blend of hospitalization & maternity insurance</p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <p>Along with Maternity & Newborn coverage, Joy offers you a number of thoughtfully designed features & Services that give you several advantages at most critical of time. Two plans available- Joy Today and Joy tomorrow.</p>
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Only 9 month & 24 month waiting period for maternity related insurance claims (including Pre Natal & Post Natal expenses) in line with your need through plans- Joy Today & Joy Tomorrow respectively</li>
                                                    <li>New born baby cover & New born birth defects</li>
                                                    <li>Health Insurance comprising of Hospitalization expenses- In patient & Day Care treatments, Pre & Post hospitalization expenses, Ambulance cover</li>
                                                    <li>Long-term policy tenure of 3 years available</li>
                                                    <li>100% increase in Sum Insured with No Claim Bonanza (optional cover)</li>
                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="buy-maternity-health-insurance-plan.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>

                            <li> <a  href="http://www.religarehealthinsurance.com/group-care-filldetails" ><img src="http://www.religarehealthinsurance.com/cpimages/group-care.png" class="header_images"><br />
                                    Group Health Insurance</a>
                                <div style="display: none; height:375px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <p>In line with our objective of ensuring good health…hamesha! Our Comprehensive Health Insurance Plan – Group Care; helps safeguard the most important asset for you i.e. your employees / Customer, against financial risks arising out of a medical emergency. </p>
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Coverage at every step with 30 Day Pre & 60 Day Post Hospitalization Cover.</li>
                                                    <li>In-Patient Hospitalization covering Room rent, doctor’s surgery, ICU charges and much more. </li>
                                                    <li>From a small stitch to a surgery – Day Care Treatments for all hospitalization less than 24 hours.</li>
                                                    <li>Domestic Road Ambulance Cover in case of an emergency.</li>
                                                    <li>Flexibility to select Sum Insured amount  & product mix basis your requirement.</li>
                                                    <li>Expenses towards Chemotherapy & Radiotherapy  are also covered. </li>
                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="http://www.religarehealthinsurance.com/group-care-filldetails" >Know More</a>           
                                    </div>

                                </div>
                            </li>






                        </ul>        
                        <div class="cl"></div>
                    </div>
                </div>
            </li>
            <li><a href="javascript:void(0);" class="tnav">Fixed Benefit Insurance</a>
                <div class="drop-menu" style="display:none; height:450px;">
                    <div class="drop-menuIn">
                        <ul class="fl cat-main">
                            <li><a href="http://www.religarehealthinsurance.com/buy-critical-illness-insurance-policy.html" class='active'><span class="sprite assureImg_png"></span><br />
                                    Critical Illness & Personal Accident Insurance</a>             

                                <div style="display: block; height:360px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>Life's uncertainties are inevitable. And that's the case with health too. With Assure, by your side, you can rest all your financial worries with Religare</p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Zero day survival period</li> 
                                                    <li>20 major Critical illness covered, Accidental Death & Permanent Total Disability also covered</li>
                                                    <li>Lump-sum payment of sum insured on diagnosis of critical illness or accidental death</li>
                                                    <li>Flexible Sum Insured options to match your financial liability</li>

                                                    <li>Avail health Checkups, every year</li>
                                                    <li>Hassle free claim settlement directly by us</li>

                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="buy-critical-illness-insurance-policy.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>

                            <li> <a href="http://www.religarehealthinsurance.com/personal-accident-insurance-policy.html" onClick="_gaq.push(['_trackEvent', 'mainnav1'])"><span class="sprite secureImg_png"></span><br />
                                    Personal Accident Insurance</a>
                                <div style="display: none; height:360px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>Truly Comprehensive Personal Accident cover with broad range of features, add-on benefits, hassle free procedures & thoughtful services </p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>In addition to basic Coverage of Accidental Death, Permanent Total & Permanent Partial Disability, Reconstructive surgery, Major Diagnostic tests, Fractures, Burns, Disapperance, Ambulance cover also covered</li> 
                                                    <li>Your Child's education also covered</li>
                                                    <li>Nursing care and Mobility cover to ease the road to recovery</li>
                                                    <li>Hassle free claim settlement directly by us</li>
                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="personal-accident-insurance-policy.html">Know More & Buy Now</a>           
                                    </div>

                                </div>
                            </li>  

                            <li> <a href="http://www.religarehealthinsurance.com/group-secure-filldetails" ><img src="http://www.religarehealthinsurance.com/cpimages/group-secure.png" class="header_images"><br />
                                    Group Personal Accident Insurance</a>
                                <div style="display: none; height:400px;" class="cat-content">
                                    <div id="category" class="fl list-column">
                                        <p>Journey of life is not smooth and some things cannot be predicted for such as an unfortunate accident. That’s why your employees/customers need Group Secure – Personal Accident Insurance so that  life’s big or small surprises never pull you down.  </p> 
                                        <div class="keyFeature">
                                            <div class="keyFeatureIn">
                                                <div class="keytxtHeader">Key Features -</div>
                                                <ul>
                                                    <li>Coverage for Accidental Death</li> 
                                                    <li>Coverage for Permanent Partial & Total Disablement and Temporary Total Disablement</li>
                                                    <li>Disappearance Cover</li>
                                                    <li>Child Education Cover </li>
                                                    <li>Hospital Cash Allowance</li>
                                                    <li>Global Coverage along with additional advantage of Terrorism coverage</li>
                                                </ul>
                                                <div class="cl"></div></div>
                                        </div>  

                                        <a class="knowbuyBtn" href="http://www.religarehealthinsurance.com/group-secure-filldetails" >Know More</a>           
                                    </div>

                                </div>
                            </li>  
                        </ul>
                        <div class="cl"></div></div>
                </div></li>
        </ul>
       
    </div>

    <div class="navigation toggleMenu"></div>
        <ul class="nav1" style="display:none;">
        <li><a href="#">Travel Insurance</a>
            <ul>
                <li><a href="http://www.religarehealthinsurance.com/travel-insurance-explore.html"><span class="sprite exploreImg_png"></span><br />
                        International Travel Insurance</a></li>
                <li><a href="http://www.religarehealthinsurance.com/travel-insurance-student-explore.html"><span class="sprite studentExploreImg_png"></span><br />
                        International Travel & Health Insurance for Students</a></li>
                <li><a href="http://www.religarehealthinsurance.com/group-explore-filldetails" class=''><img src="http://www.religarehealthinsurance.com/cpimages/group-explore.png" class="header_images"><br />
                                    Group Travel Insurance</a> </li>
            </ul>
        </li>

        <li><a href="#">Health Insurance</a>
            <ul>
                <li><a href="http://www.religarehealthinsurance.com/buy-health-insurance-policy-plan-online.html"><span class="sprite careImg_png"></span><br />
                        Comprehensive Health Insurance</a></li>
                <li><a href="http://www.religarehealthinsurance.com/buy-top-up-medical-insurance-policy.html"><span class="sprite enhanceImg_png"></span><br />
                        High Deductible Health Insurance Plan</a></li>
                <li><a href="http://www.religarehealthinsurance.com/policy-buy-senior-citizens-health-insurance-online.html"><span class="sprite careFreedomImg_png"></span><br />
                        Get Health Insurance without any Medical Check-ups</a></li>
                <li><a href="http://www.religarehealthinsurance.com/buy-maternity-health-insurance-plan.html"><span class="sprite joyImg_png"></span><br />
                        Health Insurance with Maternity & Newborn Cover</a></li>
                <li><a  href="http://www.religarehealthinsurance.com/group-care-filldetails" ><img src="http://www.religarehealthinsurance.com/cpimages/group-care.png" class="header_images"><br />
                                    Group Health Insurance</a></li>


            </ul>
        </li>
        <li><a href="#">Fixed Benefit Insurance</a>
            <ul>
                <li><a href="http://www.religarehealthinsurance.com/buy-critical-illness-insurance-policy.html"><span class="sprite assureImg_png"></span><br />
                        Critical Illness & Personal Accident Cover</a></li>
                <li><a href="http://www.religarehealthinsurance.com/personal-accident-insurance-policy.html"><span class="sprite secureImg_png"></span><br />
                        Personal Accident Cover</a></li>
                <li><a href="http://www.religarehealthinsurance.com/group-secure-filldetails" ><img src="http://www.religarehealthinsurance.com/cpimages/group-secure.png" class="header_images"><br />
                                    Group Personal Accident Insurance</a></li>

            </ul>
        </li>
        <li>
        <a href="http://www.religarehealthinsurance.com/proposalcp/renew/index-care" target="_blank" title="Renew" style="background:#b8db87; border:1px solid #b8db87;">
        <span class="icon1"></span>Renew</a></li>
            <li><a href="javascript:void(0);" title="Claims" class="claimcl_mobile" ><span class="icon2"></span>Claims</a></li>
            <li><a href="javascript:void(0);" title="Helpdesk" class="servicecl_mobile" ><span class="icon2_1"></span>Helpdesk</a></li>
            <li><a href="javascript:void(0);" title="Login" id="loginNav_mobile">Login<span class="icon3_1"></span></a><div class="clickarrow"></div>
                <ul>
                <li> <a href="https://my.religarehealthinsurance.com/">&raquo; As a Customer</a>    </li>
                <li>  <a href="https://www.religarehealthinsurance.com/agencyportal/">&raquo; As a Partner</a></li>

            </ul>
        </li>
    </ul>
        <div class="rightnavBox">

        <ul>
        <li><a href="http://www.religarehealthinsurance.com/proposalcp/renew/index-care" target="_blank" title="Renew" style="background:#b8db87; border:1px solid #b8db87;"><span class="icon1"></span>Renew</a></li>
            <li><a href="javascript:void(0);" title="Claims" class="claimcl" ><span class="icon2"></span>Claims</a><div class="clickarrow"></div></li>
            <li><a href="javascript:void(0);" title="Helpdesk" class="servicecl" ><span class="icon2_1"></span>Helpdesk</a><div class="clickarrow"></div></li>
            <li><a href="javascript:void(0);" title="Login" id="loginNav">Login<span class="icon3_1"></span></a></li>
        </ul>
    </div>

    <div class="clickarrowDiv" id="clickarrowDivClaims">
        <div class="clickarrowDivIn">
            <div class="grCross"></div>

            <div class="claimdisplaynav">
                <div class="claimselectbox">
                    <div class="claimselectboxIn">
                        <select name="select" id="selectMeClaim" class="selectMe">
                            <option value="claimstatustab">Claim Search</option>
                            <option value="claimprocesstab">Claim Process / Form</option>
                            <option value="nhospitaltab">Network Locator </option> 
                            <option value="">Dos &amp; Don&apos;ts</option>
                        </select>
                    </div>
                </div>
                <ul>
                    <li><a href="javascript:void(0);" id="claimstatus" onclick="ranvir_tab(this);" class=" headerTabs activetab">Claim Search</a></li>
                    <li><a href="javascript:void(0);" id="claimprocess" onclick="ranvir_tab(this);" class="headerTabs">Claim Process / Forms</a></li>
                    <li><a href="javascript:void(0);" id="nhospital" onclick="ranvir_tab(this);" class="headerTabs">Network Locator </a></li> 
                                            <li><a href="javascript:void(0);" id="dosdonts" onclick="ranvir_tab(this);" class="headerTabs">Dos and Don&apos;ts</a></li>
                        <li><a href="javascript:void(0);" id="clinicalp" onclick="ranvir_tab(this);" class="headerTabs">Clinical Protocols</a></li>
                                    </ul></div>

            <div class="claimdisplayContent" id="claimstatustab">
                <div class="claimdisplayContentIn"><h3>Claim Search</h3>
                    <p>To view your claim status provide us the following information:</p>
                    <div class="formBox">
                        <form name="claim_search" action="claim_search.php" method="POST">
                            <input type="text" name="clientNumber" id="clientNumber" class="claimInput" placeholder="Client No*" AUTOCOMPLETE="OFF" maxlength="10" onKeyUp="if (this.value.length > 10)
            this.value = this.value.substring(0, 10);" onKeyPress="return isNumber(event)">
                            <input type="text" name="policyNumber" id="policyNumber" class="policyInput" AUTOCOMPLETE="OFF" maxlength="8" onKeyUp="if (this.value.length > 8)
            this.value = this.value.substring(0, 8);" onKeyPress="return isNumber(event)" placeholder="Policy No*">
                            <input type="hidden" name="csrf_token" value="61850f8366579b97b352d1feafff8f6bd7be67ed49ce2e5a9a7e22c0cf333f37">
                            <input type="submit" name="claim_search" id="button" value="Go" class="goBtn" onClick="return validatePopupClaimSearch();">
                        </form>
                    </div>
                    <div class="cl"></div></div>
            </div>

            <div class="claimdisplayContent" style="display:none;" id="claimprocesstab">
                <div class="claimdisplayContentIn"><h3>Claim Process / Forms</h3>
                    <p>To download or send email claim forms please <a href="http://www.religarehealthinsurance.com/health-insurance-claim-forms.html">Click here</a></p>
                    <div class="cl"></div>
                </div>
            </div>

            <div class="claimdisplayContent" style="display:none;" id="nhospitaltab">
                <div class="claimdisplayContentIn"><h3>Network Locator </h3>
                    <p>To find our network hospital please <a href="http://www.religarehealthinsurance.com/health-plan-network-hospitals.html">Click here</a></p>
                    <div class="cl"></div></div>
            </div>

            <div class="claimdisplayContent" style="display:none;" id="dosdontstab">
                <div class="claimdisplayContentIn"><h3>Dos &amp; Don&apos;ts</h3>
                    <p>To check our dos and don'ts please <a href="http://www.religarehealthinsurance.com/dos-donts.php">click here</a></p>
                    <div class="cl"></div></div>
            </div>

            <div class="claimdisplayContent" style="display:none;" id="clinicalptab">
                <div class="claimdisplayContentIn"><h3>Clinical Protocols</h3>
                    <p>To check our clinical protocols please <a href="http://www.religarehealthinsurance.com/clinical-protocols.php">click here</a></p>
                    <div class="cl"></div></div>
            </div>


            <div class="cl"></div></div></div>

    <div class="clickarrowDiv" id="clickarrowDivServices">
        <div class="clickarrowDivIn">
            <div class="grCross"></div>

            <div class="claimdisplaynav claimdisplaynav1">
                <div class="claimselectbox">
                    <div class="claimselectboxIn">
                        <select name="select" id="selectMeService" class="selectMe">
                            <option value="tax_receiptstab">E-Policy</option>
                            <option value="customertab">Customer Support</option>

                            <option value="branch_locatortab">Branch Locator</option>
                        </select>
                    </div>
                </div>
                <ul>
                    <li><a href="javascript:void(0);" id="tax_receipts" onclick="ranvir_tabS(this);" class="activetab">E-Policy</a></li>
                    <li><a href="javascript:void(0);" id="customer" onclick="ranvir_tabS(this);" class="">Customer Support</a></li>
                    <li><a href="javascript:void(0);" id="branch_locator" onclick="ranvir_tabS(this);" class="">Branch Locator </a></li>
                </ul></div>

            <div class="claimdisplayContent claimdisplayContent1" id="tax_receiptstab">
                <div class="claimdisplayContentIn"><h3>E-Policy & Tax Receipt (80 D)</h3>
                    <p id="messagevalue" style="color:red"></p>
                    <p>To view your e-policy provide us the following information:</p>
                    <div class="formBox">
                        <form name="taxreciepts" id="taxreciepts" action="" method="post" autocomplete="OFF">
                            <input type="hidden" name="comparevalues" id="comparevalues" value="0" />
                            <div class="formBox">
                              
                                <input type="text" name="tax_policynumber" maxlength="8" id="tax_policynumber" class="policyInput" onKeyUp="if(this.value.length > 8)this.value = this.value.substring(0, 8 );" onKeyPress="return isNumber(event)" placeholder="Policy No*" value="">
                                <input type="text" name="dob" id="datepicker" value="" placeholder="DOB*" class="txtfieldFull policyInput date_uncl" maxlength="10" onFocus="if (this.value == 'DOB*') {
            this.value = '';
        }" onBlur="if (this.value == '') {
            this.value = 'DOB*';
        }" AUTOCOMPLETE="OFF"  /> 
                                <input class="txtfieldFull policyInput" type="text" id="code" name="code" AUTOCOMPLETE="OFF" value="Verification Code*" onFocus="if (this.value == 'Verification Code*') {
            this.value = '';
        }" onBlur="if (this.value == '') {
            this.value = 'Verification Code*';
        }" maxlength="4" onKeyUp="if (this.value.length > 4)
            this.value = this.value.substring(0, 4);"   />



                                <div class="grayGentxt-apply" style="width:110px; margin-top:9px;">
                                    <img id="siimage" src="securimage/securimage_show.php?sid=adf2abeb3814aafc613f6cd0b39a4810" alt="" class="fl" height="30">
                                </div>
                                <input type="submit" name="button" id="button"  value="Go" class="goBtn" onclick="return ValidateTaxReciepts();">
                            </div>
                        </form>
                    </div>
                    <div class="cl"></div></div>
            </div>

            <div class="claimdisplayContent claimdisplayContent1" style="display:none;" id="customertab">
                <div class="claimdisplayContentIn"><h3>Customer Support</h3>
                    <p>To Make a request please <a href="http://www.religarehealthinsurance.com/religare-customer-support.html">Click here</a></p>
                    <div class="cl"></div>
                </div>
            </div>

            <div class="claimdisplayContent claimdisplayContent1" style="display:none;" id="branch_locatortab">
                <div class="claimdisplayContentIn"><h3>Branch Locator</h3>
                    <p>To Find our branch please <a href="http://www.religarehealthinsurance.com/health-insurance-branch-locator.html">Click here</a></p>
                    <div class="cl"></div></div>
            </div>


            <div class="cl"></div></div></div>




    <div class="loginDiv">
        <div class="loginDivIn">
            <a href="https://my.religarehealthinsurance.com/">&raquo; As a Customer</a>    
            <a href="https://www.religarehealthinsurance.com/agencyportal/">&raquo; As a Partner</a>
            <div class="cl"></div></div></div>

    <div class="cl"></div>
</header>



<script type="text/javascript">
    GA="UA-33473576-1";
</script>
    <div class="topBanner" style="background:url(cpimages/homeImg4.jpg) top center no-repeat #ffffff; background-size:100% auto;">
        <div class="topBannerIn topbanner_iframe">


            <!--Quote Count-->
            <div class="getGuote-cont">
                <!--left-->
                <div class="getGuote-cont-left">

                    <!-- <div class="stayWorry"><span class="sprite stayWorryTxt1_png"></span></div> -->
                    <div class="cont-left-timer">
                        <!-- <img src="http://www.religarehealthinsurance.com/cpimages/bell_icon.png"> -->
                        <div class="timer_container">
                            <p>Time remaining to save tax <span id="countdown"></span></p>
                            <p class="tax_saving">Health insurance can help you save tax under section 80D</p>
                        </div>
                    </div>
                </div>
                <!--left end-->


                <!--right-->
                <div class="getGuote-cont-right">

                    <div class="getGuote-cont-right-2Minut"><span class="sprite _2minut1_png"></span></div>
                    <div class="getGuote-cont-right-GetBtn"><a href="http://www.religarehealthinsurance.com/get_quote_home.php" class="getbtn buyBtn popupboxindex"><img src="http://www.religarehealthinsurance.com/cpimages/getBtn1.gif" alt="Get Free Quote" style="border:0;"></a></div>


                </div>
                <!--right end-->

            </div>

            <div class="topPlanBox padding_bottom_0">
                <div class="travelPlanBox"><div class="travelPlanBoxIn"><!--<div class="topName"><span class="sprite travelInsuranceTxt_png"></span></div>-->
                        <div class="planContainerBox">
                            <div class="planBoxIn brdrbotwh">
                                <span class="planBoxInScroll"></span>
                                <div class="planBoxInDev">
                                    <div class="imgBox"><span class="sprite exploreImg_png"></span></div>
                                    <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">International Travel Insurance test</div></div></div>
                                </div>

                                <a href="http://www.religarehealthinsurance.com/travel-insurance-explore.html" class="planBoxOutScroll"></a>
                                <div class="planBoxInDevGr">
                                    <div class="imgBox"><span class="sprite exploreImg_png"></span></div>
                                    <ul>
                                        <li>Region specific, value for money Overseas Travel Cover providing benefits from hospitalization to trip cancellation to lost baggage & lots more.</li>
                                    </ul>
                                    <a href="http://www.religarehealthinsurance.com/travel-insurance-explore.html" class="getqBtn">Get free quote</a>
                                </div>
                            </div>

                            <div class="planBoxIn">
                                <span class="planBoxInScroll"></span>
                                <div class="planBoxInDev">
                                    <div class="imgBox"><span class="sprite studentExploreImg_png"></span></div>
                                    <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">International Travel & Health Insurance for Students</div></div></div>
                                </div>

                                <a href="http://www.religarehealthinsurance.com/travel-insurance-student-explore.html" class="planBoxOutScroll"></a>
                                <div class="planBoxInDevGr">
                                    <div class="imgBox"><span class="sprite studentExploreImg_png"></span></div>
                                    <ul>
                                        <li>A comprehensive & value for money travel insurance in alignment with university guidelines for students traveling abroad for higher education.</li>
                                    </ul>
                                    <a href="http://www.religarehealthinsurance.com/travel-insurance-student-explore.html" class="getqBtn">Get free quote</a>
                                </div></div>



                        </div>
                        <div class="cl"></div></div></div>



                <div class="travelPlanBox healthPlanBox"><div class="travelPlanBoxIn"><div class="topName">
                <!-- <img src="http://www.religarehealthinsurance.com/cpimages/healthInsuranceTxt.png" alt="Health Insurance Plan"> -->
                            <span class="sprite healthInsuranceTxt_png"></span>
                        </div>
                        <div class="planContainerBox">
                            <div class="topPlanHalf">
                                <div class="topPlanHalfIn">
                                    <div class="planBoxIn brdrbotwh">
                                        <span class="planBoxInScroll"></span>
                                        <div class="planBoxInDev">
                                            <div class="imgBox"><span class="sprite careImg_png"></span></div>
                                            <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Comprehensive Health Insurance</div></div></div>
                                        </div>

                                        <a href="http://www.religarehealthinsurance.com/buy-health-insurance-policy-plan-online.html" class="planBoxOutScroll"></a>
                                        <div class="planBoxInDevGr">
                                            <div class="imgBox"><span class="sprite careImg_png"></span></div>
                                            <ul>
                                                <li>Value for money hospitalization insurance for 1<sup>st</sup> time buyers covering basic hospital expenses with additional benefits like Automatic Recharge of SI, annual health check-up, Organ Donor & lot more.</li>
                                            </ul>
                                            <a href="http://www.religarehealthinsurance.com/buy-health-insurance-policy-plan-online.html" class="getqBtn">Get free quote</a>
                                        </div>
                                    </div>


                                    <div class="planBoxIn">
                                        <span class="planBoxInScroll"></span>
                                        <div class="planBoxInDev">
                                            <div class="imgBox"><span class="sprite careFreedomImg_png"></span></div>
                                            <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Health Insurance without Medical Check-ups</div></div></div>
                                        </div>

                                        <a href="http://www.religarehealthinsurance.com/policy-buy-senior-citizens-health-insurance-online.html" class="planBoxOutScroll"></a>
                                        <div class="planBoxInDevGr">
                                            <div class="imgBox"><span class="sprite careFreedomImg_png"></span></div>
                                            <ul>
                                                <li>Comprehensive Health Insurance to free yourself from worries of pre-policy medical check-up with benefits of Automatic SI Recharge, annual health check-up, 2 year waiting for Pre-existing diseases & more.</li>
                                            </ul>
                                            <a href="http://www.religarehealthinsurance.com/policy-buy-senior-citizens-health-insurance-online.html" class="getqBtn">Get free quote</a>
                                        </div>
                                    </div>
                                </div></div>






                            <div class="topPlanHalf">




                                <div class="planBoxIn brdrbotwh">
                                    <span class="planBoxInScroll"></span>
                                    <div class="planBoxInDev">
                                        <div class="imgBox"><span class="sprite enhanceImg_png"></span></div>
                                        <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Super Top-up Insurance</div></div></div>
                                    </div>

                                    <a href="http://www.religarehealthinsurance.com/buy-top-up-medical-insurance-policy.html" class="planBoxOutScroll"></a>
                                    <div class="planBoxInDevGr">
                                        <div class="imgBox"><span class="sprite enhanceImg_png"></span></div>
                                        <ul>
                                            <li>Value for money health insurance with benefits like annual health check-up, expert opinion to fund extreme health expenses for those who are not worried about basic health expenses.</li>
                                        </ul>
                                        <a href="http://www.religarehealthinsurance.com/buy-top-up-medical-insurance-policy.html" class="getqBtn">Get free quote</a>
                                    </div></div>

                                <div class="planBoxIn">
                                    <span class="planBoxInScroll"></span>
                                    <div class="planBoxInDev">
                                        <div class="imgBox"><span class="sprite joyImg_png"></span></div>

                                        <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Maternity & Newborn Cover with Health Insurance</div></div></div>
                                    </div>


                                    <a href="http://www.religarehealthinsurance.com/buy-maternity-health-insurance-plan.html" class="planBoxOutScroll"></a>
                                    <div class="planBoxInDevGr">
                                        <div class="imgBox"><span class="sprite joyImg_png"></span></div>
                                        <ul>
                                            <li>Maternity & newborn focused health insurance with a 9 month waiting period for maternity related expenses in line with nature’s law, especially designed for those who are planning their family.</li>
                                        </ul>
                                        <a href="http://www.religarehealthinsurance.com/buy-maternity-health-insurance-plan.html" class="getqBtn">Get free quote</a>
                                    </div></div>
                            </div> 


                        </div>
                        <div class="cl"></div></div></div>



                <div class="travelPlanBox benefitPlanBox"><div class="travelPlanBoxIn"><div class="topName"><span class="sprite benefitPlansTxt_png"></span></div>
                        <div class="planContainerBox">
                            <div class="planBoxIn brdrbotwh">
                                <span class="planBoxInScroll"></span>
                                <div class="planBoxInDev">
                                    <div class="imgBox"><span class="sprite assureImg_png"></span></div>

                                    <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Critical Illness & Personal Accident Insurance</div></div></div>
                                </div>

                                <a href="http://www.religarehealthinsurance.com/buy-critical-illness-insurance-policy.html" class="planBoxOutScroll"></a>
                                <div class="planBoxInDevGr">
                                    <div class="imgBox"><span class="sprite assureImg_png"></span></div>
                                    <ul>
                                        <li>Life is about surprises, moreover critical illnesses & accident incidents are increasing, Assure ensures fixed lumpsum payment on zero day survival & annual health check-up putting rest to financial worries.</li>
                                    </ul>
                                    <a href="http://www.religarehealthinsurance.com/buy-critical-illness-insurance-policy.html" class="getqBtn">Get free quote</a>
                                </div>
                            </div>

                            <div class="planBoxIn">
                                <span class="planBoxInScroll"></span>
                                <div class="planBoxInDev">
                                    <div class="imgBox"><span class="sprite secureImg_png"></span></div>

                                    <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Personal Accident Insurance</div></div></div>
                                </div>

                                <a href="http://www.religarehealthinsurance.com/personal-accident-insurance-policy.html" class="planBoxOutScroll"></a>
                                <div class="planBoxInDevGr">
                                    <div class="imgBox"><span class="sprite secureImg_png"></span></div>
                                    <ul>
                                        <li>Accidents always don’t mean death & hence we give the most comprehensive Personal Accident & Disability cover with additional benefits like fractures, major diagnostic tests, disappearance and lot more.</li>
                                    </ul>
                                    <a href="http://www.religarehealthinsurance.com/personal-accident-insurance-policy.html" class="getqBtn">Get free quote</a>
                                </div></div>

                        </div>
                        <div class="cl"></div></div></div>

            </div>
            <div class="group_insurance_heading">
                <h2 class="group_insurance_title">Group/Corporate Insurance</h2>
            </div>
            <div class="topPlanBox group_insurance">
                <div class="travelPlanBox"><div class="travelPlanBoxIn">
                        <div class="planContainerBox">
                            <div class="planBoxIn">
                                <span class="planBoxInScroll"></span>
                                <div class="planBoxInDev">
                                    <img src="http://www.religarehealthinsurance.com/cpimages/group-explore.png">
                                    <!-- <div class="imgBox"><span class="sprite exploreImg_png"></span></div> -->
                                    <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Group Travel Insurance</div></div></div>
                                </div>

                                <a href="http://www.religarehealthinsurance.com/group-explore-filldetails" class="planBoxOutScroll"></a>
                                <div class="planBoxInDevGr">
                                    <div class="imgBox">
                                        <img src="http://www.religarehealthinsurance.com/cpimages/group-explore.png">
                                    </div>  
                                    <!-- <div class="imgBox"><span class="sprite exploreImg_png"></span></div> -->
                                    <!-- <ul>
                                        <li>Value for money domestic and international travel insurance for your employees with medical & non-medical benefits. With benefits spanning from hospitalization to loss of passport/checked-in baggage & trip cancellation.</li>
                                    </ul> -->
                                    <a href="http://www.religarehealthinsurance.com/group-explore-filldetails" class="getqBtn">Know More</a>
                                </div>
                            </div>



                        </div>
                        <div class="cl"></div></div></div>



                <div class="travelPlanBox healthPlanBox"><div class="travelPlanBoxIn">
                        <div class="planContainerBox">
                            <div class="topPlan">
                                <div class="topPlanIn">


                                    <div class="planBoxIn">
                                        <span class="planBoxInScroll"></span>
                                        <div class="planBoxInDev">
                                            <!-- <div class="imgBox"><span class="sprite careFreedomImg_png"></span></div> -->
                                            <img src="http://www.religarehealthinsurance.com/cpimages/group-care.png">
                                            <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Group Health Insurance</div></div></div>
                                        </div>
                                        <a href="http://www.religarehealthinsurance.com/group-care-filldetails" class="planBoxOutScroll"></a>
                                        <div class="planBoxInDevGr">
                                            <div class="imgBox">
                                            <img src="http://www.religarehealthinsurance.com/cpimages/group-care.png">
                                            </div>
                                           <!--  <ul>
                                                <li>Comprehensive medical cover, which can be customized basis your customers/employee’s needs.  With features like In-Hospitalization, 540+ day care treatments, second opinion, annual health check-up and much more.</li>
                                            </ul> -->
                                            <a href="http://www.religarehealthinsurance.com/group-care-filldetails"  class="getqBtn">Know More</a>                                            
                                        </div>
                                    </div>
                                </div></div>


                        </div>
                        <div class="cl"></div></div></div>



                <div class="travelPlanBox benefitPlanBox"><div class="travelPlanBoxIn">
                        <div class="planContainerBox">
                            <div class="planBoxIn">
                                <span class="planBoxInScroll"></span>
                                <div class="planBoxInDev">
                                    <!-- <div class="imgBox"><span class="sprite assureImg_png"></span></div> -->
                                    <!-- <div class="imgBox"> -->
                                    <img src="http://www.religarehealthinsurance.com/cpimages/group-secure.png">
                                    <!-- </div> -->
                                    <div class="generalTxtBox"><div class="generalTxtBoxIn"><div class="greenTxtBox">Group Personal Accident Insurance</div></div></div>
                                </div>
                                <a href="http://www.religarehealthinsurance.com/group-secure-filldetails" class="planBoxOutScroll"></a>
                                <div class="planBoxInDevGr">                                    
                                    <div class="imgBox">
                                    <img src="http://www.religarehealthinsurance.com/cpimages/group-secure.png">
                                    </div>
                                    <!-- <ul>
                                        <li>An accident or a road mishap cannot be predicted, but we can stay prepared for it. Comprehensive personal accident cover that comes with Accidental Death & Disability Cover with additional benefits like hospital case allowance, disappearance and many more. </li>
                                    </ul> -->
                                    <a href="http://www.religarehealthinsurance.com/group-secure-filldetails" class="getqBtn">Know More</a>
                                </div>
                            </div>

                        </div>
                        <div class="cl"></div></div></div>

            </div>

<!--        <div class="stayWorry"><img src="http://www.religarehealthinsurance.com/cpimages/stayWorryTxt.png" alt="Stay worry free... Hamesha!"></div>
  <div class="stayWorryshadow"><img src="http://www.religarehealthinsurance.com/cpimages/stayWorrySh.png" alt="Stay worry free... Hamesha!"></div>-->
            <div class="cl"></div>






            <div class="cl"></div>
             
        </div>
    </div>    
    <!-- start code for pdf box -->

    <!--<div class="blackBar pdfContainer" > 
             
             <marquee  onmouseover="stop();"  scrollamount="5" onmouseout="start();" class="pdfMarquee" >
                <h2>Dear Customer, we have made certain improvements and modifications to our product ‘Care’. 
                    There is a revision in Product Features as well as Premium rates (due to the Product improvements & 
                    increased Healthcare costs). 
                    Please <a href="http://www.religarehealthinsurance.com/http://www.religarehealthinsurance.com/cpimages/Customer-Communication-Revised-Version-Care_13062016.pdf" target="_blank" style="text-decoration:none; color:#407205; font-size: 1.1em; font-weight:900;   ">click here</a>, for more details 
                </h2>
             </marquee>
     
           </div>-->


</body>



<div itemscope itemtype="http://schema.org/Organization" class="blackbar_container index_footer">
    <div class="blackBar"> 
        <a href="http://www.religarehealthinsurance.com/inc_request_callback.php?pageName=care" class="cBack callmeback_popup cboxElement"><span class="sprite_common cal_back_hover_png"></span> Request a Call Back</a>

        <a href="http://www.religarehealthinsurance.com/send_mail.php" class="cBack send_mail cboxElement"><span class="iconScnd"></span>Email</a>
    </div>

<div class="footerTop"><script src="http://www.religarehealthinsurance.com/cpjs/datepiker.js?rv=1044"></script> 
<script>
    // var DT = jQuery.noConflict();
    jQuery(function () {
        jQuery('#datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            yearRange: "-99:-0",
            dateFormat: "dd/mm/yy"

        });
    });
    jQuery(function () {

        jQuery('#from').datepicker({
            defaultDate: "0",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            minDate: $("#checkCurrentData").val(),
            dateFormat: "dd-mm-yy",
            maxDate: 180,
            onSelect: function (selectedDate) {

                var tripType = jQuery("#tripType").val();
                var fromvalue = jQuery("#from").val();
                var tovalue = jQuery("#to").val();
                if (fromvalue != '' && (fromvalue != 'Start Date')) {
                    jQuery.ajax({
                        type: "POST",
                        url: "dateCheck.php",
                        async: true,
                        data: "fromvalue=" + fromvalue + "&tripType=" + tripType + "&tovalue=" + tovalue,
                        success: function (msg) {
                            var t = msg.split(":");
                            jQuery("#noday").val(t[0]);
                            jQuery("#to").val(t[1]);
                            searchResultDate();
                        }
                    });
                }

                var currDate = $("#checkCurrentData").val();
                var currDateFormatted = changeFormat(currDate, 'd-m-y');
                var dateFormatted = changeFormat(fromvalue, 'd-m-y');
                //var maxDate = getdate(currDateFormatted, 730);
                var toDate = getdate(dateFormatted, 179);
                var toNextDate = getdate(dateFormatted, 1);
                var ftoNextDate = changeFormat(toNextDate, 'm/d/y');
                /*if (isLater(toDate, maxDate)) {
                    toDate = maxDate;
                }*/

                jQuery('#to').datepicker('option', {minDate: ftoNextDate, maxDate: new Date(toDate)});
                displayTravelSilider();
            }
        });

        jQuery('#to').datepicker({
            defaultDate: "0",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            minDate: $("#checkCurrentData").val(),
            dateFormat: "dd-mm-yy",
            maxDate: 179,
            onSelect: function (selectedDate) {

                var tripType = jQuery("#tripType").val();
                var fromvalue = jQuery("#from").val();
                var tovalue = jQuery("#to").val();

                if (fromvalue != '' && (fromvalue != 'Start Date')) {
                    jQuery.ajax({
                        type: "POST",
                        url: "dateCheck.php",
                        async: true,
                        data: "fromvalue=" + fromvalue + "&tripType=" + tripType + "&tovalue=" + tovalue,
                        success: function (msg) {
                            var t = msg.split(":");
                            jQuery("#noday").val(t[0]);
                            jQuery("#to").val(t[1]);
                            searchResultDate();
                            displayTravelSilider();
                        }
                    });
                }
            }
        });
    });

    function getdate(fromDate, noOfDays) {
        var tt = fromDate;

        var date = new Date(tt);
        var newdate = new Date(date);

        newdate.setDate(newdate.getDate() + noOfDays);

        var dd = newdate.getDate();
        var mm = newdate.getMonth() + 1;
        var y = newdate.getFullYear();

        var someFormattedDate = mm + '/' + dd + '/' + y;
        return someFormattedDate;
    }

    function changeFormat(date, format) {

        if (format == 'd-m-y') {
            var arrDates = date.split("-");
            return arrDates[1] + '/' + arrDates[0] + '/' + arrDates[2];
        }

        if (format == 'm/d/y') {
            var arrDates = date.split("/");
            return arrDates[1] + '-' + arrDates[0] + '-' + arrDates[2];
        }

        return date;
    }

    function isLater(str1, str2)
    {
        if ((new Date(str1).getTime() >= new Date(str2).getTime()))
        {
            return true;
        }
        return false;
    }
</script>
<!-- <script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/script.js?rv=1044"></script> -->
  <div class="footerTopIn">
    <ul class="navbot">
      <li class="frstul"><a href="#" class="lightGr">Why Religare?</a><span class="lightGr">Why Religare?</span>
        <ul>
            <li><span itemprop="name">Religare Health Insurance</span> (RHI), is a specialized Health Insurer offering health insurance products to employees of corporates, individual customers and financial inclusion. Launched in July’12, Religare Health Insurance has made significant progress within a short span of time, and is already operating out of 61 offices with employee strength of 2200+.<br />
            <br />
            <a href="http://www.religarehealthinsurance.com/why-religare.html" class="vMore">View More</a></li>
        </ul>
      </li>
      <li><a href="#">Quick Links</a><span>Quick Links</span>
        <ul>

        <li><a href="http://www.religarehealthinsurance.com/about-religare-health-insurance.html">About Us</a></li>
        <li><a href="http://www.religarehealthinsurance.com/career-in-religare-health-insurance.html">Careers</a></li>
        <li><a href="http://www.religarehealthinsurance.com/contact-us.html">Contact Us</a></li>
        <li><a href="http://www.religarehealthinsurance.com/proposalcp/renew/index-care" target="_blank">Policy Renewal</a></li>
		<li><a href="http://www.religarehealthinsurance.com/health-insurance-agents.html">Agent</a></li>
        <li><a href="http://www.religarehealthinsurance.com/quality_policy">Quality Policy</a></li>
        <li><a href="http://www.religarehealthinsurance.com/mission">Mission</a></li>
        <li><a href="http://www.religarehealthinsurance.com/vision">Vision</a></li>
          <li><a href="http://www.religarehealthinsurance.com/core_values">Core Values</a></li>
        </ul>
      </li>
      <li><a href="#">Customers</a><span>Customers</span>
        <ul>
        <li><a href="https://my.religarehealthinsurance.com/">Login</a></li>
        <!--<li><a href="http://www.religarehealthinsurance.com/siteupload/doc/Customer_Communication.pdf" target="_blank">Announcement</a></li>-->
        <li><a href="http://www.religarehealthinsurance.com/open-pdf.html?code=L3NpdGV1cGxvYWQvZG9jL0N1c3RvbWVyX0NvbW11bmljYXRpb24ucGRm" target="_blank">Announcement</a></li>
        <li><a href="http://www.religarehealthinsurance.com/health-insurance-claim-center.html">Claim Center</a></li>
        <li><a href="http://www.religarehealthinsurance.com/health-insurance-brochure.html">Download Center</a></li>
        <li><a href="http://www.religarehealthinsurance.com/health-plan-network-hospitals.html">Network Locator </a></li> 
        <li><a href="http://www.religarehealthinsurance.com/health-insurance-branch-locator.html">Locate a Branch</a></li>
        <li><a href="http://www.religarehealthinsurance.com/get-policy-list.html">OTP Verification</a></li>
        <li><a href="http://www.religarehealthinsurance.com/axisbanktext.php" id="axisbanktext_popup">Pay Premium @ Axis Bank</a></li>
                <li><a href="http://www.religarehealthinsurance.com/clinical-protocols.php" id="">Clinical Protocols</a></li>
        <!--<li><a href="dos-donts.php" id="">Dos &amp; Don&apos;ts</a></li>-->
                </ul>
      </li>
      <li class="lstul"><a href="#">Insurance</a><span>Insurance</span>
        <ul>
        <li><a href="http://www.religarehealthinsurance.com/policy-buy-online-mediclaim-insurance-policy.html">Mediclaim Insurance</a></li>
        <li><a href="http://www.religarehealthinsurance.com/policy-buy-senior-citizens-health-insurance-online.html">Senior Citizen Health Insurance</a></li>
        <li><a href="http://www.religarehealthinsurance.com/policy-buy-family-health-insurance-online.html">Family Health Insurance</a></li>
        <li><a href="http://www.religarehealthinsurance.com/policy-mediclaim-health-insurance-quotes.html">Health Insurance Quotes</a></li>
        <li><a href="http://www.religarehealthinsurance.com/policy-individual-health-insurance.html">Individual Health Insurance</a></li>

        <li><a href="http://www.religarehealthinsurance.com/buy-maternity-health-insurance-plan.html">Maternity Health Insurance</a></li>
		<li><a href="http://www.religarehealthinsurance.com/policy-compare-health-insurance.html">Compare Religare Health Insurance Plans</a></li>
		<li><a href="http://www.religarehealthinsurance.com/travel-insurance-explore.html">Compare Religare Travel Insurance Plans</a></li>
                <!-- <li><a href="/travelextension/">Travel Insurance Extension</a></li> -->
        </ul>
      </li>
    </ul>
  </div>
</div>
        <!-- <footer>
            <div class="footerIn">
                <div class="disclaimer"><a href="http://www.religarehealthinsurance.com/disclaimer.html">Disclaimer</a> |  <a href="privacy.html">Privacy Statement</a> | <a href="http://www.religarehealthinsurance.com/terms_conditions.html">Terms & Conditions</a> |  <a href="http://www.religarehealthinsurance.com/sitemap.html">Sitemap</a> |  <a href="http://www.religarehealthinsurance.com/religare-health-insurance-in-media.html"> Media Center</a> |  <a href="http://www.religarehealthinsurance.com/public-disclosures.html">Public Disclosures</a> |  <a href="http://www.religarehealthinsurance.com/health-bmi-calculator.html">Wellness</a> |  <a href="https://www.irda.gov.in/" target="_blank" rel="nofollow" >IRDA </a> | <a href="http://www.policyholder.gov.in" target="_blank" rel="nofollow"> Consumer Education</a> |  <a href="http://www.religarehealthinsurance.com/do-not-call.html">Do not call</a><br />
                    Insurance is the subject matter of solicitation | IRDA Registration No. 148. Copyrights 2013, All right reserved by Religare Health Insurance Company Ltd.<br /> 
                    Religare is a registered trademark of RHC Holding Private Limited used under license by Religare Enterprises Limited and its subsidiaries.<br/>
                    Reg Office - Religare Health Insurance Company Limited, 5th Floor, 19, Chawla House, Nehru Place, New Delhi-110019 | CIN - U66000DL2007PLC161503</div>

                <div class="nortonIcon"></div>

                <div class="followus">
                    <a href="https://www.facebook.com/ReligareHealthInsurance" target="_blank" class="fusIcon1"></a>
					<a href="https://plus.google.com/+ReligareHealthInsuranceIndia/posts" target="_blank" class="fusIcon3"></a>
					<a href="https://www.linkedin.com/company/religare-health-insurance" target="_blank" class="fusIcon5"></a>
					<a href="https://twitter.com/religarehealth" target="_blank" class="fusIcon7"></a>
					<a href="https://www.youtube.com/user/ReligareHealthIns" target="_blank" class="fusIcon4"></a>
                </div>
                <div class="cl"></div></div>
        </footer> -->
        <footer>
    <div class="footerIn">
        <div class="disclaimer"><a href="http://www.religarehealthinsurance.com/disclaimer.html">Disclaimer</a> |  <a href="privacy.html">Privacy Statement</a> | <a href="http://www.religarehealthinsurance.com/terms_conditions.html">Terms & Conditions</a> |  <a href="http://www.religarehealthinsurance.com/sitemap.html">Sitemap</a> |  <a href="http://www.religarehealthinsurance.com/religare-health-insurance-in-media.html"> Media Center</a> |  <a href="http://www.religarehealthinsurance.com/public-disclosures.html">Public Disclosures</a> |  <a href="http://www.religarehealthinsurance.com/health-bmi-calculator.html">Wellness</a> |  <a href="https://www.irda.gov.in/" target="_blank" rel="noopener noreferrer" rel="nofollow" >IRDA </a> | <a href="http://www.policyholder.gov.in" target="_blank" rel="noopener noreferrer nofollow"> Consumer Education</a> |  <a href="http://www.religarehealthinsurance.com/do-not-call.html">Do not call</a><br />
            Insurance is the subject matter of solicitation | IRDA Registration No. 148. Copyrights 2013, All right reserved by Religare Health Insurance Company Ltd.<br /> 
            Religare is a registered trademark of RHC Holding Private Limited used under license by Religare Enterprises Limited and its subsidiaries.<br/>
            Reg Office - Religare Health Insurance Company Limited, 5th Floor, 19, Chawla House, Nehru Place, New Delhi-110019 | CIN - U66000DL2007PLC161503<br/>
            Correspondence Address - Religare Health Insurance Company Limited, Vipul Tech Square, Tower C, 3rd Floor, Sector – 43, Golf Course Road, Gurgaon – 122009

            </div>

        <div class="nortonIcon"></div>

        <div class="followus">
            <a href="https://www.facebook.com/ReligareHealthInsurance" target="_blank" rel="noopener noreferrer" class="fusIcon1"></a>
            <a href="https://plus.google.com/+ReligareHealthInsuranceIndia/posts" target="_blank" rel="noopener noreferrer" class="fusIcon3"></a>
            <a href="https://www.linkedin.com/company/religare-health-insurance" target="_blank" rel="noopener noreferrer" class="fusIcon5"></a>
            <a href="https://twitter.com/religarehealth" target="_blank" rel="noopener noreferrer" class="fusIcon7"></a>
            <a href="https://www.youtube.com/user/ReligareHealthIns" target="_blank" rel="noopener noreferrer" class="fusIcon4"></a>
        </div>
        <div class="cl"></div></div>
</footer>        <a href="http://www.religarehealthinsurance.com/feedback_mail.php" class="feedback_send_mail"><!-- <span class="sprite feedbackimg_png"></span> --><img src="cpimages/feedback.png"></a>
   
        <div class="footerBot">
            <div class="footerBotIn">
                <a href="http://www.religarehealthinsurance.com/inc_request_callback.php?pageName=care" class="callmeback_popup"><span class="rBack"></span>Request a Call Back</a>
				<a href="tel:[18604204488]"><span class="hChoose"></span>Reach <br/> Us</a>
                <a href="http://www.religarehealthinsurance.com/send_mail.php" class="send_mail"><span class="gquote"></span>Email<br/><br/></a>
            </div>
        </div>

        <div class="popupBoxMain">
            <div class="popupBoxMainIn">
                <div class="popupInBox fl">
                    <div class="popupInBoxSection"><span></span><br />Health Insurance<br /><a href="#" class="buyBtn">Get Quote &amp; Buy</a></div>
                </div>
                <div class="cl"></div></div>
        </div>
        



<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"> <meta itemprop="streetAddress" content="Religare Health Insurance Company Limited"> <meta itemprop="addressLocality" content="New Delhi"> <meta itemprop="addressRegion" content="Delhi"> <meta itemprop="addressCountry" content="India"> <meta itemprop="postalCode" content="110019"></span> <meta itemprop="image" content="https://tractus.religarehealthinsurance.com/corporate/images/login_logo.jpg"> <meta itemprop="url" content="http://www.religarehealthinsurance.com/"> <meta itemprop="telephone" content="1860-500-4488"> <meta itemprop="email" content="customerfirst@religarehealthinsurance.com">




<script src='http://static.getclicky.com/js'></script>
    <script>try { clicky.init(101012903); } catch (e) { }</script><noscript><img height='1' width='1' src='http://static.getclicky.com/101012903ns.gif' /></noscript>

<script type='text/javascript' language='javascript'>
        document.write(unescape("%3Cscript src='" + "http://js.resulticks.com/scripts/pathanalyzer.js' type='text/javascript'%3E%3C/script%3E"));
        </script><script type='text/javascript' language='javascript'>
    fnTrackUrl('f6b50792-f5e1-49b4-83e5-0c38051cd8ac');
    </script>

<div id='notifyvisitorstag'></div>

<script>
(function(n,o,t,i,f,y) {
n[i] = function()
{ (n[i].q = n[i].q || []).push(arguments) }
; n[i].l = new Date;
n[t] = {}; n[t].auth =
{ bid_e : 'A6CB4651F874C3E1138D809ADC5066CC', bid : '4847', t : '420'}
;
n[t].async = false;
(y = o.createElement('script')).type = 'text/javascript';
y.src = "//cdn.notifyvisitors.com/js/notify-visitors-1.0.js"; 
(f = o.getElementsByTagName('script')[0]).parentNode.insertBefore(y, f);
})(window, document, 'notify_visitors', 'nv');
</script>

<script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/md5-min.js"></script>

<script>
// var containerid = "146";
// var localTime = new Date(); var gmtOffset = localTime.getTimezoneOffset() * 60 * (-1); 
// var trafficSource = document.referrer;
// var link = "https://www.notifyvisitors.com/user/container/show?brandid=4847&containerid=nv_container&gmtOffset="+gmtOffset+"&trafficSource="+trafficSource;
// var iframe = document.createElement('iframe'); iframe.frameBorder=0; iframe.style.backgroundColor='transparent'; 
// iframe.width="1349px"; iframe.height="167px"; iframe.id="nv_container_iframe"; iframe.setAttribute("src", link);
// document.getElementById("nv_container").appendChild(iframe); 
</script>

<script>
function pushNotification(){
    var tot_premium = $('#total_premium').text();
    var no_of_years = $("input[type='radio'][name='tenure']:checked").val();
    var plan_type = 'care';
    var mobile = $('#mobile').val();
    var suminsured = $('#sumInsured').val();

    if(suminsured >= 10000000 ){
      plan_type = 'Care Global Plus';
    }else if(suminsured >= 5000000){
      plan_type = 'Care Global';
    }else if(suminsured >= 1500000){
      plan_type = 'Care Elite Plus';
    } else if(suminsured >= 500000){
      plan_type = 'Care Elite';
    } else{
      plan_type = 'Care Super Saver';
    }

    var sum_insured = suminsured/100000+" Lacs";

    var encrypt_data = {encrypt:{mobile:mobile, code:'notify-'+mobile}};
    var encrypted_mobile, encrypted_code, product_url;

    $.ajax({
        type: "POST", 
        url: "../proposalcp/newdesign/aes_encryption.php",
        async:false,
        data: $("#select_skin_form_9").serialize(),
        success: function(msg){
            product_url = 'http://www.religarehealthinsurance.com/notificationPage.php?details='+encodeURI(msg);
          console.log(msg);
            msg = JSON.parse(msg);
            encrypted_mobile =   msg.mobile;
            encrypted_code = msg.code;
            console.log(encodeURI(encrypted_code));
            console.log(encodeURI(encrypted_mobile));
            
            console.log(product_url);       
            
        }
  }); 

    nv('event', 'REGISTER',{name:'', premium:tot_premium, tenure:no_of_years, plan:plan_type, mobile:encrypted_mobile, suminsured:sum_insured,url:product_url}, 10, 1);
    console.log({name:'', premium:tot_premium, tenure:no_of_years, plan:plan_type, mobile:encrypted_mobile, suminsured:sum_insured,url:product_url});
    // window.open(product_url, '_blank')
}
</script>   
</div>



    <!--[if lt IE 9]>
<script src="http://www.religarehealthinsurance.com/cpjs/html5.js"></script>
<![endif]-->

<!-- <script src="http://www.religarehealthinsurance.com/cpjs/jquery-1.12.1.min.js?rv=1044" type="text/javascript"></script>
    <script>
        var $j = jQuery.noConflict(true);
    </script> -->
<script src="http://www.religarehealthinsurance.com/cpjs/jquery-ui.min.js?rv=1044"></script>
<script src="http://www.religarehealthinsurance.com/cpjs/touchpunch.js?rv=1044"></script>
<script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/placeholders.js?rv=1044"></script>    
<script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/claim_srch.js?rv=1044"></script>

<!-- <script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/intlTelInput.min.js?rv=1044"></script>
<script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/utils.js?rv=1044"></script> -->
<!-- for using new jquery -->

<script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/script.js?rv=1044"></script>

<script>

    // var DT = jQuery.noConflict();
    jQuery(function () {
        jQuery('#datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-99:-0",
            dateFormat: "dd/mm/yy"

        });
    });
</script>

<script src="http://www.religarehealthinsurance.com/cpjs/jquery.colorbox.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#getQuote").hide();
        $(".example2").colorbox({width: "1000px", inline: true, href: "#inline_example2"});
        var width = $(window).width(), height = $(window).height();
        if (width > 1010) {
            $(".popupboxindex").colorbox({width: "1010px", height: "900px", iframe: true}); // for home popup health and travel
        } else
        {
            $(".popupboxindex").colorbox({width: "98%", height: "1500px", iframe: true}); // for home popup health and travel
        }
        if (width > 700) {
            $("#sendhospitalemail").colorbox({width: "45%", inline: true, href: "#inline_example6"});
            $(".example3").colorbox({width: "45%", inline: true, href: "#inline_example3"});
            $(".example2").colorbox({width: "1000px", height: "900px", iframe: true});
            $(".callmeback_popup").colorbox({width: "45%", height: "350px", iframe: true});
            $(".send_mail").colorbox({width: "45%", height: "650px", iframe: true});
            $("#renewtext_popup").colorbox({width: "600px", height: "650px", iframe: false});
            $("#axisbanktext_popup").colorbox({width: "600px", height: "650px", iframe: false});
            $(".feedback_send_mail").colorbox({width: "45%", height: "650px", iframe: true});
            $(".provideo").colorbox({width: "60%", height: "500px", inline: true, href: "#inline_provideo"});
            $(".provideo1").colorbox({width: "60%", height: "500px", inline: true, href: "#inline_provideo1"});
            $(".provideo2").colorbox({width: "60%", height: "500px", inline: true, href: "#inline_provideo2"});
            $(".provideo3").colorbox({width: "60%", height: "500px", inline: true, href: "#inline_provideo3"});
            $(".provideo4").colorbox({width: "60%", height: "500px", inline: true, href: "#inline_provideo4"})
        } else
        {
            $("#sendhospitalemail").colorbox({width: "98%", inline: true, href: "#inline_example6"});
            $(".example3").colorbox({width: "98%", inline: true, href: "#inline_example3"});
            $(".callmeback_popup").colorbox({width: "98%", height: "380px", iframe: true});
            $(".send_mail").colorbox({width: "98%", height: "680px", iframe: true});
            $("#renewtext_popup").colorbox({width: "300px", height: "650px", iframe: false});
            $("#axisbanktext_popup").colorbox({width: "300px", height: "650px", iframe: false});
            $(".feedback_send_mail").colorbox({width: "98%", height: "680px", iframe: true});

            $(".provideo").colorbox({width: "98%", height: "500px", inline: true, href: "#inline_provideo"});
            $(".provideo1").colorbox({width: "98%", height: "500px", inline: true, href: "#inline_provideo1"});
            $(".provideo2").colorbox({width: "98%", height: "500px", inline: true, href: "#inline_provideo2"});
            $(".provideo3").colorbox({width: "98%", height: "500px", inline: true, href: "#inline_provideo3"});
            $(".provideo4").colorbox({width: "98%", height: "500px", inline: true, href: "#inline_provideo4"});
        }
    });
</script>


<script type="text/javascript">
    $(document).ready(function () {





        $('.ulnav li a.tnav').click(function () {
            if
                    ($(this).parent().hasClass("liactive")) {
                $('.liactive').parent().children().children('.drop-menu').hide(".drop-menu");
                $('.liactive').removeClass('liactive');
                $('.selected').removeClass('selected');

            } else {
                $(this).parent().parent().children().children(".selected").removeClass("selected");
                $(this).parent().parent().children().children('.drop-menu').hide();
                $(this).parent().parent().parent().children().children('li').removeClass('liactive');


                $(this).addClass("selected");
                $(this).parent().children(".drop-menu").show();
                $(this).parent('li').addClass('liactive');



                $("#clickarrowDivClaims , .clickarrow").hide();
                $("#clickarrowDivServices , .clickarrow").hide();


                $('.loginDiv').hide();
            }
        });


        $('.cat-main li a').mouseover(function () {
            $(this).parent().parent().children().children('.active').removeClass('active');
            $(this).addClass("active");
            $(this).parent().parent().children().children('.cat-content').hide();
            $(this).parent().children(".cat-content").show();
        });


        $('.tnav ,.drop-menu').click(function (event) {
            $('html').one('click', function () {
                $('.drop-menu').hide();
                $('.tnav').removeClass('selected');
                $('.ulnav li').removeClass('liactive');
            });
            event.stopPropagation();
        });












        setTimeout(function () {
            $("#rightFloatInTollFreeNumber").stop().animate({
                right: 155
            }, 200);
        }, 4000);

        setInterval(function () {
            $("#rightFloatInTollFreeNumber").stop().animate({
                right: 42
            }, 200);
        }, 10000);


        $('.rightFloat div').mouseover(function () {
            $(this).stop().animate({
                right: 155
            }, 200);
        }).mouseout(function () {
            $(this).stop().animate({
                right: 42
            }, 200)
        });

        $(".grCross").click(function (event) {
            $('.clickarrow, .clickarrowDiv').hide();

        });

        function popoutCallnow() {
            $("#rightFloatInRequestCallBack").stop().animate({
                right: 155
            }, 200);
            setTimeout( function(){ 
                popinCallnow();
            }  , 3000 );
        }
        function popinCallnow() {
            $("#rightFloatInRequestCallBack").stop().animate({
                right: 42
            }, 200);
        }
        popoutCallnow();
        setInterval(popoutCallnow, 30000);


        $('.planBoxInScroll').mouseover(function () {
            $(".planBoxInScroll").show();
            $(".planBoxInDev").show();
            $(".planBoxOutScroll").hide();
            $(".planBoxInDevGr").hide();


            $(this).parent().children("div").toggle();
            $(this).parent().children(".planBoxInScroll").toggle();
                $(this).parent().children(".planBoxOutScroll").toggle();

        });





        $('.planBoxOutScroll').mouseout(function () {
            $(this).parent().children("div").toggle();
            $(this).parent().children(".planBoxInScroll").toggle();
            $(this).parent().children(".planBoxOutScroll").toggle();
        });

        $('#selectMeClaim').change(function () {
            $('.claimdisplayContent').hide();
            $('#' + $(this).val()).show();
        })
        $('#selectMeService').change(function () {
            $('.claimdisplayContent1').hide();
            $('#' + $(this).val()).show();
        })
    });

    $(".rightnavBox ul li a.claimcl").click(function () {
        $("#messagevalue").html('');
        $('#clickarrowDivServices,.clickarrow').hide();
        $(this).parent().children("div").show();
        $('#clickarrowDivClaims').show();
        $('.drop-menu').hide();


    });

    $(".rightnavBox ul li a.servicecl").click(function () {
        $("#messagevalue").html('');
        $('#clickarrowDivClaims,.clickarrow').hide();
        $(this).parent().children("div").show();
        $('#clickarrowDivServices').show();


    });

    $("#loginNav").click(function () {
        $('#clickarrowDivServices,#clickarrowDivClaims, .clickarrow, .service_tabs').hide();
        $('.drop-menu').hide();
        $('.tnav').removeClass('selected');
        $('.ulnav li').removeClass('liactive');
    });

    $('.navigation, .nav1').click(function (event) {
        $('html').one('click', function () {
            $('.nav1').hide();
            $('.toggleMenu').toggleClass("activenav");
        });
        event.stopPropagation();
    });



    $(".rightnavBox ul li #loginNav").click(function () {
        $('.loginDiv').toggle();
    });


    $('#loginNav, .loginDiv').click(function (event) {
        $('html').one('click', function () {
            $('.loginDiv').hide();
        });

        event.stopPropagation();
    });



    function ranvir_tab(en)
    {
        var tabcont_id = $(en).attr("id");

        $(".claimdisplayContent").hide();
        $("#" + tabcont_id + "tab").show();


        $(".headerTabs").removeClass("activetab");
        $(".claimstatus ul li a").removeClass("activetab");
        $(en).addClass("activetab");

    }

    function ranvir_tabS(en)
    {
        var tabcont_id = $(en).attr("id");
        $("#tax_receiptstab,#customertab,#branch_locatortab").hide();
        $("#" + tabcont_id + "tab").show();

        $(".claimdisplaynav1 ul li a").removeClass("activetab");
        $(en).addClass("activetab");

    }
</script>







<script>
    jQuery(document).ready(function () {
        var placeholder = 'placeholder' in document.createElement('input');
        if (!placeholder) {
            $.getScript("http://www.religarehealthinsurance.com/cpjs/placeholders.js", function () {
                $(":input").each(function () {   // this will work for all input fields
                    $(this).placeHolder();
                });
            });
        }
        var aliceImageHost = '';
        var globalConfig = {};
        $(".servicecl_mobile").click(function () {
            $("a.servicecl").trigger("click");
        });
        $(".claimcl_mobile").click(function () {
            $("a.claimcl").trigger("click");
        });
        $("#loginNav_mobile").click(function () {
            $("#loginNav").trigger("click");
        });

        var messagevalue = $("#messagevalue").html();
        if (messagevalue != '')
        {
            // jQuery methods go here...
            $("#customertab, #branch_locatortab").hide();
            $("#clickarrowDivServices, #tax_receiptstab").show();
            $("a.servicecl").next().show();

            $(".claimdisplaynav1 ul li a").removeClass("activetab");
            $("#tax_receipts").addClass("activetab");
        }

        function ValidateTaxReciepts()
        {
            var tax_policynumber = $("#tax_policynumber").val();
            var datepicker = $("#datepicker").val();
            var code = $("#code").val();
            $("#messagevalue").text('');
            if ($.trim(tax_policynumber) == 'Policy No*')
            {
                alert("Please Enter Policy Number");
                $("#tax_policynumber").focus();
                return false;
            }
            if ($.trim(datepicker) == 'DOB*')
            {
                alert("Please Enter Date of Birth!");
                $("#datepicker").focus();
                return false;
            }
            if ($.trim(code) == 'Verification Code*' || code == '')
            {
                alert("Please Enter Verification Code");
                $("#code").focus();
                return false;
            }
        }


    });

</script>

<script type="text/javascript">

    $(".servicecl_mobile").click(function () {
        $("a.servicecl").trigger("click");
    });
    $(".claimcl_mobile").click(function () {
        $("a.claimcl").trigger("click");
    });
    $("#loginNav_mobile").click(function () {
        $("#loginNav").trigger("click");
    });
</script>


<script type="text/javascript">
    var _raq = _raq || [];
    _raq.push(['_setAccount', '579df61c88e4111071b21e30']);
    _raq.push(['_setData', {}]);
    (function () {
        var ra = document.createElement('script');
        ra.type = 'text/javascript';
        ra.async = true;
        ra.src = "//analytics.religarehealthinsurance.com/" + "ra.js";
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ra, s);
    })();
</script>
<script type="text/javascript" src="http://www.religarehealthinsurance.com/cpjs/iframeResizer.min.js"></script> 

<script type="text/javascript">
    (function () {
        var hm = document.createElement('script');
        hm.type = 'text/javascript';
        hm.async = true;
        hm.src = ('++u-heatmap-it+log-js').replace(/[+]/g, '/').replace(/-/g, '.');
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(hm, s);
    })();

    if($.browser.msie){
        $('input[placeholder]').each(function(){
            var input = $(this);
            $(input).val(input.attr('placeholder'));
            $(input).focus(function(){
            if (input.val() == input.attr('placeholder')) {
            input.val('');
            }
            });
            $(input).blur(function(){
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.val(input.attr('placeholder'));
            }
            });
        });
      };

      $('.menu_toggle').click(function(){
        $('.lefnNavContainer').toggleClass('slide_out');
        $('.black_overlay').toggleClass('show');
      })

      $('.sub_menu_toggle').click(function(){
        $('.care_tab_ul').toggleClass('slide_out');
        $('.black_overlay').toggleClass('show');
      })
</script>    <script type="text/javascript">
        function checkOffset() {
            if ($(window).height() + $(window).scrollTop() -40>= $('.footerTop').offset().top) {
                $('.blackBar').css({'position': 'absolute', 'top': 0, 'bottom': 'auto'});
            } else {
                $('.blackBar').css({'position': 'fixed', 'bottom': 0, 'top': 'auto'}); // restore when you scroll up        
            }
        }
        checkOffset();
        
            $(document).scroll(function () {
                checkOffset();
            });   
            // iFrameResize({
            //     minHeight: 100
            // });

</script>


<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 981036149;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/981036149/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script>(function (w, d, t, r, u) {
        var f, n, i;
        w[u] = w[u] || [], f = function () {
            var o = {ti: "4049149"};
            o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
        }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () {
            var s = this.readyState;s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)}, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)})(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4049149&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<img src="//bat.bing.com/action/0?ti=4049149&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" />
<form id="submitProdForm" method="POST" action='http://www.religarehealthinsurance.com/religarecp/group_care.php'>
    <input type="hidden" id="prodName" name="prodName" value="">
</form>

<script type="text/javascript">
    function submitGroup(prodName){
        $("#prodName").val(prodName);
        if(prodName=="group_care_page"){
            $("#submitProdForm").attr("action","http://www.religarehealthinsurance.com/group-care-filldetails")
        }else if(prodName == "group_secure_page"){
            $("#submitProdForm").attr("action","http://www.religarehealthinsurance.com/group-secure-filldetails")
        }else{
            $("#submitProdForm").attr("action","http://www.religarehealthinsurance.com/group-explore-filldetails")
        }
        $("#submitProdForm").submit();
    }

    $('.group_insurance_title').one('click',function(){
        var scrolled = $(document).scrollTop() + 160;
        $('html,body').animate({
            scrollTop: scrolled},
        'slow');
    })
</script>
</body>
</html>